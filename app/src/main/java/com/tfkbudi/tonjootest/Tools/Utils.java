package com.tfkbudi.tonjootest.Tools;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.DisplayMetrics;

/**
 * Created by TFKBUDI on 27/01/2017.
 */

public class Utils {

    public static void showProgress(ProgressDialog dialog, String content) {
        dialog.setTitle("Please Wait");
        dialog.setMessage(content);
        dialog.setIndeterminate(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    public static void hideProgress(ProgressDialog dialog) {
        dialog.cancel();
    }

    public static int getWidth(Context context){
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return metrics.widthPixels;


    }

    public static int getHeight(Context context){
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return metrics.heightPixels;
    }
}
