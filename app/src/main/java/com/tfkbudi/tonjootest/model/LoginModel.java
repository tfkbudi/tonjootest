package com.tfkbudi.tonjootest.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by TFKBUDI on 10/08/2017.
 */

public class LoginModel {


    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("user_id")
    @Expose
    private Integer userId;

    public Integer getSuccess() {
        return success;
    }

    public String getToken() {
        return token;
    }

    public Integer getUserId() {
        return userId;
    }

}
