package com.tfkbudi.tonjootest;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.orhanobut.hawk.Hawk;
import com.tfkbudi.tonjootest.Tools.HawkKey;
import com.tfkbudi.tonjootest.Tools.Url;
import com.tfkbudi.tonjootest.Tools.Utils;
import com.tfkbudi.tonjootest.model.LoginModel;

import org.json.JSONException;
import org.json.JSONObject;
/*tes staged merge */
/*ngualang soal merge
sekarang pake git-cola*/

public class LoginActivity extends AppCompatActivity {
    EditText edtUsername,edtPassword;
    Button btnLogin;
    ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();
        Hawk.init(this).build();
        mProgressDialog = new ProgressDialog(this);

    }

    public void initView(){
        edtUsername = (EditText) findViewById(R.id.edt_username);
        edtPassword = (EditText) findViewById(R.id.edt_password);
        btnLogin = (Button) findViewById(R.id.btn_Login);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                doLogin();

            }
        });
    }

    public void doLogin(){
        Utils.showProgress(mProgressDialog,"Sign in...");
        StringRequest jsonObjReq = new StringRequest(Request.Method.POST,
                Url.urlLogin,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Utils.hideProgress(mProgressDialog);

                        consumeResponse(response);

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Volley e", "Error: " + error.toString());
                Utils.hideProgress(mProgressDialog);
            }
        }) ;

        TonjooTestApplication.getInstance().addToRequestQueue(jsonObjReq);
    }

    public void consumeResponse(String response){

        String nResponse = response.substring(0,19)+","+response.substring(19,response.length());

        Gson gson = new GsonBuilder().create();
        LoginModel login = gson.fromJson(nResponse,LoginModel.class);

        if(login.getSuccess() == 1){
            Hawk.put(HawkKey.token,login.getToken());
            startActivity(new Intent(this,MainActivity.class));
        }else{
            Toast.makeText(this,"please check again username & password",Toast.LENGTH_SHORT).show();
        }
    }


}
