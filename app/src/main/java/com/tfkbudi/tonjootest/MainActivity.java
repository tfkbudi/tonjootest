package com.tfkbudi.tonjootest;

import android.app.ProgressDialog;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.orhanobut.hawk.Hawk;
import com.tfkbudi.tonjootest.Tools.HawkKey;
import com.tfkbudi.tonjootest.Tools.Url;
import com.tfkbudi.tonjootest.Tools.Utils;
import com.tfkbudi.tonjootest.adapter.ContactAdapter;
import com.tfkbudi.tonjootest.model.ContactModel;
import com.tfkbudi.tonjootest.model.LoginModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
/*
taufik*/
public class MainActivity extends AppCompatActivity {

    Toolbar toolbar;
    RecyclerView recyclerView;
    List<ContactModel.DataContact> contactList;
    List<ContactModel.DataContact> contactPage;
    ContactAdapter adapter;
    String token;

    Button btnNext,btnPrevious;
    int page = 0;
    int index = 0;

    ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Hawk.init(this).build();

        token = Hawk.get(HawkKey.token);
        //getData();
        initView();

    }

    public void initView(){
        mProgressDialog = new ProgressDialog(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Contact List");

        btnNext = (Button) findViewById(R.id.btnNext);
        btnPrevious = (Button) findViewById(R.id.btnPrevious);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //nextFunction();
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        getData();


    }

    public void getData(){
        Utils.showProgress(mProgressDialog,"get contact...");
        StringRequest jsonObjReq = new StringRequest(Request.Method.GET,
                Url.urlContactList,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {

                        Utils.hideProgress(mProgressDialog);
                        consumeResponse(response);
                        //Log.e("debug res",response);

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Volley", "Error: " + error.toString());
                Utils.hideProgress(mProgressDialog);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer "+token);
                return headers;
            }
        } ;

        TonjooTestApplication.getInstance().addToRequestQueue(jsonObjReq);
    }

    public void consumeResponse(String response){

        Gson gson = new GsonBuilder().create();
        ContactModel model = gson.fromJson(response,ContactModel.class);

        if(model.getSuccess() == 1){
            contactList = model.getData();
            adapter = new ContactAdapter(MainActivity.this,contactList);
            recyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }

    }


}
