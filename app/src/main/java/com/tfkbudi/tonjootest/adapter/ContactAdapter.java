package com.tfkbudi.tonjootest.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.tfkbudi.tonjootest.R;
import com.tfkbudi.tonjootest.model.ContactModel;

import java.util.List;

/**
 * Created by TFKBUDI on 10/08/2017.
 */

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.MyViewHolder> {

    private List<ContactModel.DataContact> contactList;
    private Context context;

    public ContactAdapter(Context context, List<ContactModel.DataContact> contactList){
        this.context = context;
        this.contactList = contactList;

    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView txtName,txtGender,txtEmail;
        ImageView imgThumb;

        public MyViewHolder(View view){
            super(view);
            txtName = (TextView) view.findViewById(R.id.txt_name);
            txtGender = (TextView) view.findViewById(R.id.txt_gender);
            txtEmail = (TextView) view.findViewById(R.id.txt_email);
            imgThumb = (ImageView) view.findViewById(R.id.imgThumb);

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_contact, parent, false);

        return new ContactAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ContactModel.DataContact model = contactList.get(position);

        holder.txtName.setText(model.getFirstName()+" - "+model.getLastName());
        holder.txtGender.setText(model.getGender());
        holder.txtEmail.setText(model.getEmail());
        Glide.with(context).load(model.getAvatar()).into(holder.imgThumb);

    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }
}
